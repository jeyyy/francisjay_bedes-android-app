package com.activity.bedescontacttracing;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class GeneralInformation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_information);
    }

    /** Called when the user taps the Next Button in General Information */
    public void next(View view) {
        Intent generalIntent = new Intent(this, HealthInformation.class);
        EditText full_name = findViewById(R.id.fullName);
        EditText phone_number = findViewById(R.id.phoneNumber);
        EditText city_resident = findViewById(R.id.city);

        generalIntent.putExtra("FullName",full_name.getText().toString());
        generalIntent.putExtra("PhoneNumber",phone_number.getText().toString());
        generalIntent.putExtra("City",city_resident.getText().toString());

        startActivity(generalIntent);
    }
}