package com.activity.bedescontacttracing;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class HealthInformation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_information);
    }

    /** Called when the user taps the Next Button in Health Information */
    public void next(View view) {

        Intent healthIntent = new Intent(this, Confirmation.class);

        Intent generalIntent = getIntent();

        EditText question1 = findViewById(R.id.quest2);
        EditText question2 = findViewById(R.id.quest1);

        healthIntent.putExtra("Question1",question1.getText().toString());
        healthIntent.putExtra("Question2",question2.getText().toString());

        /** PASS THE VALUES FROM GENERAL TO CONFIRMATION*/
        healthIntent.putExtra("FullName",generalIntent.getStringExtra("FullName"));
        healthIntent.putExtra("PhoneNumber",generalIntent.getStringExtra("PhoneNumber"));
        healthIntent.putExtra("City",generalIntent.getStringExtra("City"));

        startActivity(healthIntent);
    }
}