package com.activity.bedescontacttracing;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Confirmation extends AppCompatActivity {

    TextView full_name;
    TextView phone_number;
    TextView city_resident;

    TextView question1;
    TextView question2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        full_name = findViewById(R.id.fname);
        phone_number = findViewById(R.id.phone);
        city_resident = findViewById(R.id.city);
        question1 = findViewById(R.id.q1);
        question2 = findViewById(R.id.q2);

        Intent healthIntent =getIntent();

        full_name.setText(healthIntent.getStringExtra("FullName").toUpperCase());
        phone_number.setText(healthIntent.getStringExtra("PhoneNumber").toUpperCase());
        city_resident.setText(healthIntent.getStringExtra("City").toUpperCase());
        question1.setText(healthIntent.getStringExtra("Question1").toUpperCase());
        question2.setText(healthIntent.getStringExtra("Question2").toUpperCase());

    }
    public void next(View view)
    {
        Intent Intent = new Intent(this, Success.class);
        startActivity(Intent);
    }
}